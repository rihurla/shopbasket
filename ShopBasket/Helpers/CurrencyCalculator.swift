//
//  CurrencyCalculator.swift
//  ShopBasket
//
//  Created by Ricardo Hurla on 08/12/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import Foundation

struct CurrencyCalculator {
  
  static func convert(value: Double, currency type: CurrencyType, currency rate: CurrencyRate) -> Double {
    let exchangeRate: Double
    
    switch type {
    case .americanDolar:
      exchangeRate = rate.rates.USD
    case .britishPound:
      exchangeRate = rate.rates.GBP
    case .brazilianReal:
      exchangeRate = rate.rates.BRL
    case .euro:
      exchangeRate = 1.0 // Euro is the base currency
    }
    
    return value * exchangeRate
  }
}
