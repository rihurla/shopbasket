//
//  CurrencyType.swift
//  ShopBasket
//
//  Created by Ricardo Hurla on 08/12/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import Foundation

enum CurrencyType {
  case euro
  case americanDolar
  case britishPound
  case brazilianReal
}
