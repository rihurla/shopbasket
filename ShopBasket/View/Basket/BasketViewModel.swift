//
//  BasketViewModel.swift
//  ShopBasket
//
//  Created by Ricardo Hurla on 08/12/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import UIKit

protocol BasketViewModelDelegate: class {
  func viewModel(_ viewModel: BasketViewModel, willDisplayRemovalAlertFor item: BasketItem)
  func viewModelDidUpdateItems(_ viewModel: BasketViewModel)
  func viewModelDidUpdateItemQuantity(_ viewModel: BasketViewModel)
  func viewModel(_ viewModel: BasketViewModel, didFailWith error: Error)
}

class BasketViewModel: NSObject {
  
  var basketItems: [BasketItem]
  weak var delegate: BasketViewModelDelegate?
  var currencyRequester: CurrencyRequestManaging
  var currencyRate: CurrencyRate?
  
  init(basketItems: [BasketItem]) {
    self.basketItems = basketItems
    self.currencyRequester = CurrencyRequestManager()
    super.init()
  }
  
  func requestLatestCurrency() {
    currencyRequester.requestLatestRate(successHandler: { (currencyRate) in
      self.currencyRate = currencyRate
    }) { (error) in
      self.delegate?.viewModel(self, didFailWith: error)
    }
  }
  
  func totalValueFromBasket() -> Double {
    let prices = basketItems.map({ $0.price })
    return prices.reduce(0.0, +)
  }
  
  func removeItemFromBasket(item: BasketItem) {
    basketItems.removeAll(where: { $0 == item })
    delegate?.viewModelDidUpdateItems(self)
  }
  
  func convertTotalTo(type: CurrencyType) -> Double {
    let total = self.totalValueFromBasket()
    guard let rate = currencyRate else { return total }
    return CurrencyCalculator.convert(value: total, currency: type, currency: rate)
  }
  
  func checkoutIsAvailable() -> Bool {
    return !self.basketItems.isEmpty
  }
  
}

extension BasketViewModel: UITableViewDataSource {
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: BasketItemTableViewCell.basketReusableIdentifier) as! BasketItemTableViewCell
    cell.setUp(basketItem: basketItems[indexPath.item])
    cell.selectionStyle = .none
    cell.delegate = self
    return cell
    
  }
  
  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return basketItems.count
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return UITableView.automaticDimension
  }
  
}

extension BasketViewModel: BasketItemTableViewCellDelegate {
  func cellDidUpdate(_ cell: BasketItemTableViewCell) {
    delegate?.viewModelDidUpdateItemQuantity(self)
  }
  
  func cell(_ cell: BasketItemTableViewCell, shouldRemove item: BasketItem) {
    delegate?.viewModel(self, willDisplayRemovalAlertFor: item)
  }
}
