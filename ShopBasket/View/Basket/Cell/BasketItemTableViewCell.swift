//
//  BasketItemTableViewCell.swift
//  ShopBasket
//
//  Created by Ricardo Hurla on 08/12/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import UIKit

protocol BasketItemTableViewCellDelegate: class {
  func cellDidUpdate(_ cell: BasketItemTableViewCell)
  func cell(_ cell: BasketItemTableViewCell, shouldRemove item: BasketItem)
}

class BasketItemTableViewCell: UITableViewCell {
  
  weak var delegate: BasketItemTableViewCellDelegate?
  static let basketReusableIdentifier = "BasketItemCell"
  var basketItem: BasketItem?

  @IBOutlet weak var productNameLabel: UILabel!
  @IBOutlet weak var productUnitDescriptionLabel: UILabel!
  @IBOutlet weak var priceLabel: UILabel!
  @IBOutlet weak var quantitylabel: UILabel!
  
  override func awakeFromNib() {
    super.awakeFromNib()
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
  }
  
  func setUp(basketItem: BasketItem) {
    productNameLabel.text = basketItem.product.name
    productUnitDescriptionLabel.text = basketItem.product.unit
    quantitylabel.text = "\(basketItem.quantity)"
    let format = NSLocalizedString("eur_currency_price_label", comment: "gbp currency placeholder")
    priceLabel.text = String.localizedStringWithFormat(format, basketItem.price)
    self.basketItem = basketItem
  }
    
  @IBAction func didTapAddButton(_ sender: UIButton) {
    guard let item = basketItem else { return }
    item.quantity += 1
    setUp(basketItem: item)
    self.delegate?.cellDidUpdate(self)
  }
  
  @IBAction func didTapRemoveButton(_ sender: UIButton) {
    guard let item = basketItem else { return }
    if item.quantity > 1 {
      item.quantity -= 1
      setUp(basketItem: item)
      self.delegate?.cellDidUpdate(self)
    } else {
      self.delegate?.cell(self, shouldRemove: item)
    }
  }
  
}
