//
//  BasketViewController.swift
//  ShopBasket
//
//  Created by Ricardo Hurla on 08/12/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import UIKit

class BasketViewController: UIViewController {
  
  let viewModel: BasketViewModel
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var totalPriceLabel: UILabel!
  @IBOutlet weak var checkoutContainerView: UIView!
  @IBOutlet weak var checkoutTotalPriceLabel: UILabel!
  @IBOutlet weak var checkoutButton: UIButton!
  
  
  init(viewModel: BasketViewModel) {
    self.viewModel = viewModel
    super.init(nibName: "BasketViewController", bundle: Bundle.main)
    self.viewModel.delegate = self
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    configureCheckoutView()
    configureTableView()
    setTotalPrice()
    setCheckoutTotalPrice()
  }
  
  // MARK: Configuration
  func configureCheckoutView() {
    let format = NSLocalizedString("EUR_currency_format", comment: "eur currency format")
    checkoutTotalPriceLabel.text = String.localizedStringWithFormat(format, viewModel.totalValueFromBasket())
    checkoutContainerView.isHidden = true
    let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.hideCheckoutView))
    tapGestureRecognizer.numberOfTapsRequired = 1
    checkoutContainerView.addGestureRecognizer(tapGestureRecognizer)
    checkoutButton.isEnabled = viewModel.checkoutIsAvailable()
  }
  
  func configureTableView() {
    tableView.dataSource = viewModel
    tableView.estimatedRowHeight = 55
    tableView.rowHeight = UITableView.automaticDimension
    tableView.tableFooterView = UIView.init()
    let basketCellNib = UINib.init(nibName: "BasketItemTableViewCell", bundle: nil)
    tableView.register(basketCellNib, forCellReuseIdentifier: BasketItemTableViewCell.basketReusableIdentifier)
  }
  
  // MARK: UI Updates
  func setTotalPrice() {
    let format = NSLocalizedString("total_price_format", comment: "total price placeholder")
    totalPriceLabel.text = String.localizedStringWithFormat(format, viewModel.totalValueFromBasket())
  }
  
  func refresh() {
    tableView.reloadData()
    setTotalPrice()
    setCheckoutTotalPrice()
    checkoutButton.isEnabled = viewModel.checkoutIsAvailable()
  }
  
  func setCheckoutTotalPrice() {
    let convertedValue = viewModel.convertTotalTo(type: .euro)
    let format = NSLocalizedString("EUR_currency_format", comment: "eur currency format")
    let total = String.localizedStringWithFormat(format, convertedValue)
    checkoutTotalPriceLabel.text = total
  }
  
  // MARK: Gesture Recognizer
  @objc func hideCheckoutView(recognizer: UITapGestureRecognizer) {
    checkoutContainerView.isHidden = true
  }

  // MARK: Button Actions
  @IBAction func didTapCloseButton(_ sender: Any) {
    self.dismiss(animated: true) {}
  }
  
  @IBAction func didTapCheckout(_ sender: UIButton) {
    viewModel.requestLatestCurrency()
    checkoutContainerView.isHidden = false
  }
  
  // Currencies
  @IBAction func didTapConvertToEUR(_ sender: UIButton) {
    let convertedValue = viewModel.convertTotalTo(type: .euro)
    let format = NSLocalizedString("EUR_currency_format", comment: "eur currency format")
    let total = String.localizedStringWithFormat(format, convertedValue)
    checkoutTotalPriceLabel.text = total
  }
  
  @IBAction func didTapConvertToUSD(_ sender: UIButton) {
    let convertedValue = viewModel.convertTotalTo(type: .americanDolar)
    let format = NSLocalizedString("USD_currency_format", comment: "eur currency format")
    let total = String.localizedStringWithFormat(format, convertedValue)
    checkoutTotalPriceLabel.text = total
  }
  
  @IBAction func didTapConvertToGBP(_ sender: UIButton) {
    let convertedValue = viewModel.convertTotalTo(type: .britishPound)
    let format = NSLocalizedString("GBP_currency_format", comment: "eur currency format")
    let total = String.localizedStringWithFormat(format, convertedValue)
    checkoutTotalPriceLabel.text = total
  }
  
  @IBAction func didTapConvertToBRL(_ sender: UIButton) {
    let convertedValue = viewModel.convertTotalTo(type: .brazilianReal)
    let format = NSLocalizedString("BRL_currency_format", comment: "eur currency format")
    let total = String.localizedStringWithFormat(format, convertedValue)
    checkoutTotalPriceLabel.text = total
  }
  
  // MARK: Alerts
  func displayRemovalAlertFor(item: BasketItem) {
    
    let format = NSLocalizedString("alert_item_removal_format", comment: "format")
    let message = String.localizedStringWithFormat(format, item.product.name)
    
    let alert = UIAlertController(title: NSLocalizedString("alert_title", comment: "alert title"),
                                  message: message,
                                  preferredStyle: .alert)
    
    alert.addAction(UIAlertAction(title: NSLocalizedString("alert_cancel_option", comment: "cancel option"),
                                  style: .cancel, handler: { action in }))
    
    alert.addAction(UIAlertAction(title: NSLocalizedString("alert_yes_option", comment: "yes option"),
                                  style: .destructive, handler: { action in
                                    self.viewModel.removeItemFromBasket(item: item)
                                    let notificationName = Notification.Name(ProductListViewModel.itemRemovalNotification)
                                    NotificationCenter.default.post(name: notificationName,
                                                                    object: nil,
                                                                    userInfo: ["item":item])
    }))
    
    self.present(alert, animated: true, completion: nil)
  }
  
  func displayErrorAlertWith(message: String) {

    let alert = UIAlertController(title: NSLocalizedString("alert_title_error", comment: "alert title"),
                                  message: message,
                                  preferredStyle: .alert)
    
    alert.addAction(UIAlertAction(title: NSLocalizedString("alert_default_option", comment: "cancel option"),
                                  style: .default, handler: { action in }))
    
    self.present(alert, animated: true, completion: nil)
  }
  
}

// MARK: ViewModel Delegate
extension BasketViewController: BasketViewModelDelegate {
  func viewModel(_ viewModel: BasketViewModel, didFailWith error: Error) {
    displayErrorAlertWith(message: error.localizedDescription)
  }
  
  func viewModelDidUpdateItemQuantity(_ viewModel: BasketViewModel) {
    setTotalPrice()
    setCheckoutTotalPrice()
  }
  
  func viewModelDidUpdateItems(_ viewModel: BasketViewModel) {
    refresh()
  }
  
  func viewModel(_ viewModel: BasketViewModel, willDisplayRemovalAlertFor item: BasketItem) {
    displayRemovalAlertFor(item: item)
  }
}
