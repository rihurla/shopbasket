//
//  ProductTableViewCell.swift
//  ShopBasket
//
//  Created by Ricardo Hurla on 08/12/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import UIKit

class ProductTableViewCell: UITableViewCell {
  
  static let reusableIdentifier = "ProductCell"

  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var unitDescriptionLabel: UILabel!
  @IBOutlet weak var priceLabel: UILabel!
  
  override func awakeFromNib() {
    super.awakeFromNib()
  }
  
  func setUp(product: Product) {
    let format = NSLocalizedString("eur_currency_price_label", comment: "gbp currency placeholder")
    priceLabel.text = String.localizedStringWithFormat(format, product.price)
    nameLabel.text = product.name
    unitDescriptionLabel.text = product.unit.uppercased()
  }

}
