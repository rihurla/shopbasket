//
//  ProductListViewModel.swift
//  ShopBasket
//
//  Created by Ricardo Hurla on 08/12/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import UIKit

protocol ProductListViewModelDelegate: class {
  func viewModel(_ viewModel: ProductListViewModel, didSelectItemAt indexPath: IndexPath)
}

class ProductListViewModel: NSObject {
  
  static let itemRemovalNotification = "itemRemovalNotification"
  weak var delegate: ProductListViewModelDelegate?
  let products: [Product]
  var basketItems = [BasketItem]()
  let defaultCurrency = "GBP"
  
  override init() {
    products = ProductFactory.generateProducts()
    super.init()
    let notificationName = Notification.Name(ProductListViewModel.itemRemovalNotification)
    NotificationCenter.default.addObserver(self,
                                           selector: #selector(removeBasketItem(notification:)),
                                           name: notificationName,
                                           object: nil)
  }
  
  func createUpdateBasketItemWith(product: Product) {
    let basketItem = BasketItem(product: product, quantity: 1, currency: defaultCurrency)
    if basketItemExists(item: basketItem), let item = basketItems.filter({ $0 == basketItem}).first {
      item.quantity += 1
    } else {
      basketItems.append(basketItem)
    }
  }
  
  func basketItemExists(item: BasketItem) -> Bool {
    return basketItems.contains(where: { $0 == item })
  }
  
  @objc func removeBasketItem(notification: NSNotification) {
    if let basketItem =  notification.userInfo?["item"] as? BasketItem {
      basketItems.removeAll(where: { $0 == basketItem })
    }
  }
  
}

extension ProductListViewModel: UITableViewDelegate {
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let product = products[indexPath.item]
    createUpdateBasketItemWith(product: product)
    self.delegate?.viewModel(self, didSelectItemAt: indexPath)
  }
}

extension ProductListViewModel: UITableViewDataSource {
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: ProductTableViewCell.reusableIdentifier) as! ProductTableViewCell
    cell.setUp(product: products[indexPath.item])
    cell.selectionStyle = .none
    return cell
    
  }
  
  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return products.count
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return UITableView.automaticDimension
  }
  
}

