//
//  ViewController.swift
//  ShopBasket
//
//  Created by Ricardo Hurla on 08/12/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import UIKit

class ProductListViewController: UIViewController {
  
  let viewModel = ProductListViewModel()
  var basketBarButton: UIBarButtonItem?
  @IBOutlet weak var tableView: UITableView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.title = NSLocalizedString("product_list_vc_title", comment: "view controller title")
    self.viewModel.delegate = self
    configureNavBar()
    configuraNavBarItem()
    configureTableView()
  }
  
  func configureNavBar() {
    self.navigationController?.navigationBar.prefersLargeTitles = true
  }
  
  func configuraNavBarItem() {
    let icon = UIImage.init(named: "basket_icon")?.withRenderingMode(.alwaysTemplate)
    let basketButton = UIButton()
    basketButton.setImage(icon, for: .normal)
    basketButton.addTarget(self, action:#selector(self.displayBasket), for: .touchUpInside)
    basketButton.frame = CGRect(x: 0, y: 0, width: 36, height: 36)
    
    let widthConstraint = basketButton.widthAnchor.constraint(equalToConstant: 36)
    let heightConstraint = basketButton.heightAnchor.constraint(equalToConstant: 36)
    heightConstraint.isActive = true
    widthConstraint.isActive = true
    
    let barButton = UIBarButtonItem(customView: basketButton)
    self.navigationItem.rightBarButtonItem = barButton
    basketBarButton = barButton
  }
  
  func configureTableView() {
    tableView.delegate = viewModel
    tableView.dataSource = viewModel
    tableView.estimatedRowHeight = 55
    tableView.rowHeight = UITableView.automaticDimension
    tableView.tableFooterView = UIView.init()
    let productCellNib = UINib.init(nibName: "ProductTableViewCell", bundle: nil)
    tableView.register(productCellNib, forCellReuseIdentifier: ProductTableViewCell.reusableIdentifier)
  }
  
  @objc func displayBasket() {
    let basketVM = BasketViewModel.init(basketItems: viewModel.basketItems)
    let basketVC = BasketViewController.init(viewModel: basketVM)
    self.present(basketVC, animated: true, completion: nil)
  }
  
  func animateBasketButton() {
    guard let barButton = basketBarButton, let customView = barButton.customView else { return }
    UIView.animate(withDuration: 0.2, animations: {
      customView.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
    }) { (finished) in
      if finished {
        UIView.animate(withDuration: 0.2, animations: {
          customView.transform = CGAffineTransform.identity
        })
      }
    }
  }

}

extension ProductListViewController: ProductListViewModelDelegate {
  func viewModel(_ viewModel: ProductListViewModel, didSelectItemAt indexPath: IndexPath) {
    self.animateBasketButton()
  }
}

