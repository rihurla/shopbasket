//
//  ProductFactory.swift
//  ShopBasket
//
//  Created by Ricardo Hurla on 08/12/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import Foundation

struct ProductFactory {
  static func generateProducts() -> [Product] {
    let coffee = Product(name: "Coffee", price: 2.30, unit: "per 240g")
    let tea = Product(name: "Tea", price: 3.10, unit: "per box of 60")
    let sugar = Product(name: "Sugar", price: 2.00, unit: "per 1kg")
    let milk = Product(name: "Milk", price: 1.20, unit: "per bottle")
    let cup = Product(name: "Cup", price: 0.50, unit: "per unit")
    return [coffee, tea, sugar, milk, cup]
  }
}
