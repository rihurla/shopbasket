//
//  Product.swift
//  ShopBasket
//
//  Created by Ricardo Hurla on 08/12/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import Foundation

struct Product: Equatable {
  let name: String
  let price: Double
  let unit: String
}
