//
//  BasketItem.swift
//  ShopBasket
//
//  Created by Ricardo Hurla on 08/12/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import Foundation

class BasketItem: Equatable {
  let product: Product
  var quantity: Int
  let currency: String
  var price: Double {
    get {
      return product.price * Double(quantity)
    }
  }
  
  init(product: Product, quantity: Int, currency: String = "EUR") {
    self.product = product
    self.quantity = quantity
    self.currency = currency
  }
}

extension BasketItem {
  static func == (lhs: BasketItem, rhs: BasketItem) -> Bool {
    return lhs.product == rhs.product
  }
}
