//
//  CurrencyRequestManager.swift
//  ShopBasket
//
//  Created by Ricardo Hurla on 08/12/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import Foundation
import Alamofire
import CodableAlamofire

class CurrencyRequestManager: NSObject, CurrencyRequestManaging {
  
  let serviceUrls = ServiceUrls()
  
  func requestLatestRate(successHandler: @escaping (CurrencyRate) -> Void, errorHandler: @escaping (Error) -> Void) {
    guard let url = URL(string: serviceUrls.latestRatesUrl()) else {
      errorHandler(ShopBasketError.runtimeError("URL failed to be created"))
      return
    }
    let decoder = JSONDecoder()
    
    Alamofire.request(url).responseDecodableObject(keyPath: nil,
                                                   decoder: decoder) { (response: DataResponse<CurrencyRate>) in
                                                    switch response.result {
                                                    case .success:
                                                      if let currency = response.result.value {
                                                        successHandler(currency)
                                                      } else {
                                                        let errorMessage = "Rates not available"
                                                        errorHandler(ShopBasketError.runtimeError(errorMessage))
                                                      }
                                                    case .failure(let error):
                                                      errorHandler(error)
                                                    }
    }
  }
  
}
