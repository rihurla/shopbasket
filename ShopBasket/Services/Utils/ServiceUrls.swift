//
//  ServiceUrls.swift
//  ShopBasket
//
//  Created by Ricardo Hurla on 08/12/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import Foundation

struct ServiceUrls {
  private let baseUrl = "http://data.fixer.io/api/"
  private let latest = "latest"
  private let accessKey = "?access_key="
  private let baseCurrency = "&base=GBP" // Not supported with free subcription
  private let symbols = "&symbols=USD,GBP,BRL"
  
  func latestRatesUrl() -> String {
    return baseUrl + latest + accessKey + ServiceCredentials.apiAcessKey + symbols
  }
}
