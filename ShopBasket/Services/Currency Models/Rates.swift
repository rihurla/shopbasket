//
//  Rates.swift
//  ShopBasket
//
//  Created by Ricardo Hurla on 08/12/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import Foundation

struct Rates: Codable {
  let USD: Double
  let GBP: Double
  let BRL: Double
}
