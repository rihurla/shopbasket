//
//  CurrencyCalculatorTests.swift
//  ShopBasketTests
//
//  Created by Ricardo Hurla on 08/12/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import XCTest
@testable import ShopBasket

class CurrencyCalculatorTests: XCTestCase {
  
  let unitUnderTest = CurrencyCalculator.self
  
  override func setUp() {
    super.setUp()
  }
  
  override func tearDown() {
    super.tearDown()
  }
  
  func testCurrencyConversion() {
    let expectedValue = 11.382460000000002
    let rates = Rates(USD: 1.138246, GBP: 0.894075, BRL: 4.447472)
    let currencyRate =  CurrencyRate(base: "EUR", date: "2018-12-08", rates: rates)
    let convertedValue = unitUnderTest.convert(value: 10.0, currency: .americanDolar, currency: currencyRate)
    XCTAssertEqual(convertedValue, expectedValue)
  }
}
