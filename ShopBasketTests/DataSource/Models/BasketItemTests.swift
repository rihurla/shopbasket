//
//  BasketItemTests.swift
//  ShopBasketTests
//
//  Created by Ricardo Hurla on 08/12/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import XCTest
@testable import ShopBasket

class BasketItemTests: XCTestCase {
  
  override func setUp() {
    super.setUp()
  }
  
  override func tearDown() {
    super.tearDown()
  }
  
  func testBasketItemCreation() {
    let expectedProduct = Product(name: "test_product", price: 5.00, unit: "per bottle")
    let unitUnderTest = BasketItem(product: expectedProduct, quantity: 1, currency: "GBP")
    XCTAssertEqual(unitUnderTest.product, expectedProduct)
  }
  
  func testBasketItemUpdate() {
    let expectedQuantity = 2
    let product = Product(name: "test_product", price: 5.00, unit: "per bottle")
    let unitUnderTest = BasketItem(product: product, quantity: 1, currency: "GBP")
    unitUnderTest.quantity += 1
    XCTAssertEqual(unitUnderTest.quantity, expectedQuantity)
  }
  
  func testBasketItemPrice() {
    let expectedPrice = 5.00
    let product = Product(name: "test_product", price: 5.00, unit: "per bottle")
    let unitUnderTest = BasketItem(product: product, quantity: 1, currency: "GBP")
    XCTAssertEqual(unitUnderTest.price, expectedPrice)
  }
  
  func testBasketItemUpdatedPrice() {
    let expectedPrice = 10.00
    let product = Product(name: "test_product", price: 5.00, unit: "per bottle")
    let unitUnderTest = BasketItem(product: product, quantity: 1, currency: "GBP")
    unitUnderTest.quantity += 1
    XCTAssertEqual(unitUnderTest.price, expectedPrice)
  }

}
