//
//  ProductTests.swift
//  ShopBasketTests
//
//  Created by Ricardo Hurla on 08/12/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import XCTest
@testable import ShopBasket

class ProductTests: XCTestCase {

  override func setUp() {
    super.setUp()
  }
  
  override func tearDown() {
    super.tearDown()
  }
  
  func testProductCreation() {
    let expectedProductName = "test_product"
    let expectedProductPrice = 5.00
    let expectedProductUnit = "per bottle"
    let unitUnderTest = Product(name: expectedProductName, price: expectedProductPrice, unit: expectedProductUnit)
    XCTAssertEqual(unitUnderTest.name, expectedProductName)
    XCTAssertEqual(unitUnderTest.price, expectedProductPrice)
    XCTAssertEqual(unitUnderTest.unit, expectedProductUnit)
  }

}
