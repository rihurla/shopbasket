//
//  ProductFactoryTests.swift
//  ShopBasketTests
//
//  Created by Ricardo Hurla on 08/12/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import XCTest
@testable import ShopBasket

class ProductFactoryTests: XCTestCase {

  let unitUnderTest = ProductFactory.self
  
  override func setUp() {
    super.setUp()
  }
  
  override func tearDown() {
   super.tearDown()
  }
  
  func testProductGeneration() {
    let expectedProductCount = 5
    let products = unitUnderTest.generateProducts()
    XCTAssertEqual(products.count, expectedProductCount)
  }

}
