//
//  ProductListViewModelTests.swift
//  ShopBasketTests
//
//  Created by Ricardo Hurla on 08/12/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import XCTest
@testable import ShopBasket

class ProductListViewModelTests: XCTestCase {
  
  let unitUnderTest = ProductListViewModel()
  
  override func setUp() {
    super.setUp()
  }
  
  override func tearDown() {
    super.tearDown()
    unitUnderTest.basketItems.removeAll()
  }
  
  func testGeneratedProducts() {
    let expectedProductsCount = 5
    let productsCount = unitUnderTest.products.count
    XCTAssertEqual(productsCount, expectedProductsCount)
  }
  
  func testCreateBasketItem() {
    let expectedBasketItemCount = 1
    let product = Product(name: "test_product", price: 5.00, unit: "per bottle")
    unitUnderTest.createUpdateBasketItemWith(product: product)
    XCTAssertEqual(unitUnderTest.basketItems.count, expectedBasketItemCount)
  }
  
  func testUpdateBasketItem() {
    let expectedBasketItemCount = 1
    let expectedProductQuantity = 2
    let product = Product(name: "test_product", price: 5.00, unit: "per bottle")
    unitUnderTest.createUpdateBasketItemWith(product: product) // Adding product
    unitUnderTest.createUpdateBasketItemWith(product: product) // Updating existing product count
    XCTAssertEqual(unitUnderTest.basketItems.count, expectedBasketItemCount)
    XCTAssertEqual(unitUnderTest.basketItems.first?.quantity, expectedProductQuantity)
  }
  
  func testBasketItemExists() {
    let expectedResult = true
    let product = Product(name: "test_product", price: 5.00, unit: "per bottle")
    let basketItem = BasketItem(product: product, quantity: 1, currency: "GBP")
    unitUnderTest.basketItems.append(basketItem)
    let itemExists = unitUnderTest.basketItemExists(item: basketItem)
    XCTAssertEqual(itemExists, expectedResult)
  }
  
  func testBasketItemDoesNotExist() {
    let expectedResult = false
    let product = Product(name: "test_product", price: 5.00, unit: "per bottle")
    let basketItem = BasketItem(product: product, quantity: 1, currency: "GBP")
    let itemExists = unitUnderTest.basketItemExists(item: basketItem)
    XCTAssertEqual(itemExists, expectedResult)
  }

}
