//
//  BasketViewModelTests.swift
//  ShopBasketTests
//
//  Created by Ricardo Hurla on 08/12/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import XCTest
@testable import ShopBasket

class BasketViewModelTests: XCTestCase {

  override func setUp() {
    super.setUp()
  }
  
  override func tearDown() {
    super.tearDown()
  }
  
  func testTotalValueFromBasket() {
    let expectedTotalPrice = 7.00
    let product = Product(name: "test_product", price: 5.00, unit: "per bottle")
    let basketItem = BasketItem(product: product, quantity: 1, currency: "GBP")
    let product2 = Product(name: "test_product_2", price: 2.00, unit: "per bottle")
    let basketItem2 = BasketItem(product: product2, quantity: 1, currency: "GBP")
    let unitUnderTest = BasketViewModel(basketItems: [basketItem, basketItem2])
    let totalPrice = unitUnderTest.totalValueFromBasket()
    XCTAssertEqual(totalPrice, expectedTotalPrice)
  }
  
  func testRemoveItemFromBasket() {
    let expectedItemCount = 1
    let product = Product(name: "test_product", price: 5.00, unit: "per bottle")
    let basketItem = BasketItem(product: product, quantity: 1, currency: "GBP")
    let product2 = Product(name: "test_product_2", price: 2.00, unit: "per bottle")
    let basketItem2 = BasketItem(product: product2, quantity: 1, currency: "GBP")
    let unitUnderTest = BasketViewModel(basketItems: [basketItem, basketItem2])
    unitUnderTest.removeItemFromBasket(item: basketItem2)
    let basketItemsCount = unitUnderTest.basketItems.count
    XCTAssertEqual(basketItemsCount, expectedItemCount)
  }

}
